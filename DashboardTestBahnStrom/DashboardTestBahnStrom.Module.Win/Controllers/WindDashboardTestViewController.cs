﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.DashboardWin;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DashboardTestBahnStrom.Module.BusinessObjects;
namespace DashboardTestBahnStrom.Module.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class WindDashboardTestViewController : ViewController<DetailView>
    {
        public WindDashboardTestViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
            TargetObjectType = typeof(IDashboardData);            
        }

        private void Application_ObjectSpaceCreated(object sender, ObjectSpaceCreatedEventArgs e)
        {
            NonPersistentObjectSpace nonPersistentObjectSpace = e.ObjectSpace as NonPersistentObjectSpace;
            if (nonPersistentObjectSpace != null)
            {
                nonPersistentObjectSpace.ObjectsGetting += NonPersistentObjectSpace_ObjectsGetting; ;
            }
        }

        private void NonPersistentObjectSpace_ObjectsGetting(object sender, ObjectsGettingEventArgs e)
        {
            if (((IDashboardData)View.CurrentObject).Title == "ABC")
            {
                using (var os = Application.CreateObjectSpace(typeof(NonPersistentTestDashboard)))
                {
                    if (e.ObjectType == typeof(NonPersistentTestDashboard))
                    {
                        NonPersistentTestDashboard obj = new NonPersistentTestDashboard();

                        obj.Name = "Ben";
                        obj.Cash = 1234;
                        obj.Oid = Guid.NewGuid();
                        var list = new List<NonPersistentTestDashboard>();
                        list.Add(obj);
                        e.Objects = list;
                    }
                }
            }
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            if (((IDashboardData)View.CurrentObject).Title == "ABC")
            {
                Application.ObjectSpaceCreated += Application_ObjectSpaceCreated;
            }           
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            
        }
        protected override void OnDeactivated()
        {            
            base.OnDeactivated();
            Application.ObjectSpaceCreated -= Application_ObjectSpaceCreated;
        }
    }
}
